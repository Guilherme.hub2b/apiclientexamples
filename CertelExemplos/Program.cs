﻿using CertelExemplos.ApiClient.Auth;
using CertelExemplos.ApiClient.Categories;
using System;
using System.Threading.Tasks;

namespace CertelExemplos
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            await TokenApiExample();
            Console.WriteLine("==================");
            await CategoryApiExample();

            Console.ReadLine();
        }

        private static async Task CategoryApiExample()
        {
            // https://hub2bapi.docs.apiary.io/#reference/0/pedidos/buscar-categorias
            ICategoryApi categoryApi = new CategoryApi();

            /*
             * Busca todas as categorias existentes
             */

            var allCategories = await categoryApi.FetchCategories();
            Console.WriteLine("Buscando todas as categorias cadastradas:");
            Console.WriteLine($"Total de categorias encontradas: {allCategories.Count}");
            allCategories.ForEach(c => Console.WriteLine($"[{c.Code}] {c.Name}"));


            /*
             * Busca uma categoria especifica
             */
            var categoryCode = "101"; // example
            var singleCategory = await categoryApi.GetSingleCategory(categoryCode);
            Console.WriteLine($"\r\nBuscando apenas a categoria de id {categoryCode}:");
            Console.WriteLine($"[{singleCategory.Code}] {singleCategory.Name}");
        }

        private static async Task TokenApiExample()
        {
            IAuthApi auth = new AuthApi();

            /*
             * Criando um novo accessToken:
             */

            var tokenResponse = await auth.GetToken();
            Console.WriteLine("Dados de acesso gerados:");
            Console.WriteLine($"AccessToken: {tokenResponse.AccessToken}");
            Console.WriteLine($"RefreshToken: {tokenResponse.RefreshToken}");

            /*
             *O accessToken possui duração de 2 horas. Após esse período, ele deve ser renovado utilizando o refreshToken gerado.
             * https://gitlab.com/hub2b/hub2b.api.docs/-/blob/master/authentications.md
             * Exemplo abaixo:
             */

            var currentRefreshToken = tokenResponse.RefreshToken;
            var refreshResponse = await auth.RefreshToken(currentRefreshToken);
            Console.WriteLine("\r\nDados de acesso Renovados:");
            Console.WriteLine($"Novo AccessToken: {refreshResponse.AccessToken}");
            Console.WriteLine($"Novo RefreshToken: {refreshResponse.RefreshToken}");
        }
    }
}
