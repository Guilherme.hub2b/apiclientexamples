﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertelExemplos.ApiClient.Categories.Models
{
    public partial class CategoryResponsePage
    {
        [JsonProperty("error", NullValueHandling = NullValueHandling.Ignore)]
        public string Error { get; set; }

        [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("list", NullValueHandling = NullValueHandling.Ignore)]
        public List<Category> List { get; set; }

        [JsonProperty("paging", NullValueHandling = NullValueHandling.Ignore)]
        public Paging Paging { get; set; }
    }

    public partial class Category
    {
        [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("parentCode")]
        public string ParentCode { get; set; }
    }

    public partial class Paging
    {
        [JsonProperty("total", NullValueHandling = NullValueHandling.Ignore)]
        public long? Total { get; set; }

        [JsonProperty("pages", NullValueHandling = NullValueHandling.Ignore)]
        public int Pages { get; set; }

        [JsonProperty("currentPage", NullValueHandling = NullValueHandling.Ignore)]
        public int CurrentPage { get; set; }

        [JsonProperty("perPage", NullValueHandling = NullValueHandling.Ignore)]
        public int PerPage { get; set; }
    }
}
