﻿using CertelExemplos.ApiClient.Categories.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CertelExemplos.ApiClient.Categories
{
    public class CategoryApi : ICategoryApi
    {
        private readonly IRestClient restClient;
        
        private readonly string accountIdTenant = "2572";
        private readonly string accountToken = "bG9qYXNjZXJ0ZWw6cHdk";

        public CategoryApi()
        {
            this.restClient = new RestClient();
            this.restClient.BaseUrl = new Uri("https://eb-api.plataformahub.com.br/RestServiceImpl.svc");
            this.restClient.Timeout = -1;
        }

        public async Task<List<Category>> FetchCategories()
        {
            var categories = new List<Category>();
            var offset = 0;
            var limit = 20;
            var currentPage = 1;
            var totalPages = 2;

            do
            {
                var request = new RestRequest();
                request.Resource = $"listcategories/{this.accountIdTenant}";
                request.AddHeader("auth", accountToken);
                request.AddParameter("offset", offset);
                request.AddParameter("limit", limit);

                IRestResponse response = await restClient.ExecuteGetAsync(request);
                var responseObj = JsonConvert.DeserializeObject<CategoryResponsePage>(response.Content);

                categories.AddRange(responseObj.Data.List);
                currentPage = responseObj.Data.Paging.CurrentPage;
                totalPages = responseObj.Data.Paging.Pages;

                offset += limit;
            } while (currentPage < totalPages);

            return categories;
        }

        public async Task<Category> GetSingleCategory(string categoryCode)
        {
            var request = new RestRequest();
            request.Resource = $"listcategories/{this.accountIdTenant}";
            request.AddHeader("auth", accountToken);
            request.AddParameter("code", categoryCode);
            
            IRestResponse response = await restClient.ExecuteGetAsync(request);
            var responseObj = JsonConvert.DeserializeObject<CategoryResponsePage>(response.Content);

            return responseObj.Data.List.FirstOrDefault();
        }
    }
}
