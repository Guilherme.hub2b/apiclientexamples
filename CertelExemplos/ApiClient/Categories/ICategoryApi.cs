﻿using CertelExemplos.ApiClient.Categories.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CertelExemplos.ApiClient.Categories
{
    public interface ICategoryApi
    {
        Task<List<Category>> FetchCategories();
        Task<Category> GetSingleCategory(string categoryCode);
    }
}
