﻿using CertelExemplos.ApiClient.Auth.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CertelExemplos.ApiClient.Auth
{
    public class AuthApi : IAuthApi
    {
        private readonly IRestClient restClient;
        private readonly string clientId = "eZnNgspFZUl49bj0nFvO9QqVkPIuaq";
        private readonly string clientSecret = "IN7jjpXRneizBA9nGsvc7ZtregtQml";

        private readonly string accountUsername = "testes@certel";
        private readonly string accountPassword = "rFgyZL";
        public AuthApi()
        {
            this.restClient = new RestClient();
            this.restClient.BaseUrl = new Uri("https://rest.hub2b.com.br/oauth2");
            this.restClient.Timeout = -1;
        }

        public async Task<AccessTokenResponse> GetToken()
        {
            var request = new RestRequest();
            request.Resource = "login";
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", GetCreateTokenContent(), ParameterType.RequestBody);
            IRestResponse response = await restClient.ExecutePostAsync(request);

            return JsonConvert.DeserializeObject<AccessTokenResponse>(response.Content);
        }

        public async Task<AccessTokenResponse> RefreshToken(string currentRefreshToken)
        {
            var request = new RestRequest();
            request.Resource = "token";
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", GetRefreshTokenContent(currentRefreshToken), ParameterType.RequestBody);
            IRestResponse response = await restClient.ExecutePostAsync(request);

            return JsonConvert.DeserializeObject<AccessTokenResponse>(response.Content);
        }

        private string GetCreateTokenContent()
        {
            return JsonConvert.SerializeObject(new CreateTokenContent()
            {
                ClientId = this.clientId,
                ClientSecret = this.clientSecret,
                GrantType = "password",
                Scope = "inventory orders catalog",
                Username = this.accountUsername,
                Password = this.accountPassword
            });
        }

        private string GetRefreshTokenContent(string currentRefreshToken)
        {
            return JsonConvert.SerializeObject(new RefreshTokenContent()
            {
                ClientId = this.clientId,
                ClientSecret = this.clientSecret,
                GrantType = "refresh_token",
                RefreshToken = currentRefreshToken
            });
        }
    }
}
