﻿using CertelExemplos.ApiClient.Auth.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CertelExemplos.ApiClient.Auth
{
    public interface IAuthApi
    {
        Task<AccessTokenResponse> GetToken();
        Task<AccessTokenResponse> RefreshToken(string currentRefreshToken);

    }
}
